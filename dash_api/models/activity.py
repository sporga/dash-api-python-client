# coding: utf-8

"""
    Dash API

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)  # noqa: E501

    OpenAPI spec version: v1
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


import pprint
import re  # noqa: F401

import six

from dash_api.models.activity_type import ActivityType  # noqa: F401,E501


class Activity(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'id': 'int',
        'name': 'str',
        'parent': 'Activity',
        'type': 'ActivityType'
    }

    attribute_map = {
        'id': 'id',
        'name': 'name',
        'parent': 'parent',
        'type': 'type'
    }

    def __init__(self, id=None, name=None, parent=None, type=None):  # noqa: E501
        """Activity - a model defined in Swagger"""  # noqa: E501

        self._id = None
        self._name = None
        self._parent = None
        self._type = None
        self.discriminator = None

        if id is not None:
            self.id = id
        self.name = name
        if parent is not None:
            self.parent = parent
        self.type = type

    @property
    def id(self):
        """Gets the id of this Activity.  # noqa: E501


        :return: The id of this Activity.  # noqa: E501
        :rtype: int
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this Activity.


        :param id: The id of this Activity.  # noqa: E501
        :type: int
        """

        self._id = id

    @property
    def name(self):
        """Gets the name of this Activity.  # noqa: E501


        :return: The name of this Activity.  # noqa: E501
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """Sets the name of this Activity.


        :param name: The name of this Activity.  # noqa: E501
        :type: str
        """
        if name is None:
            raise ValueError("Invalid value for `name`, must not be `None`")  # noqa: E501

        self._name = name

    @property
    def parent(self):
        """Gets the parent of this Activity.  # noqa: E501


        :return: The parent of this Activity.  # noqa: E501
        :rtype: Activity
        """
        return self._parent

    @parent.setter
    def parent(self, parent):
        """Sets the parent of this Activity.


        :param parent: The parent of this Activity.  # noqa: E501
        :type: Activity
        """

        self._parent = parent

    @property
    def type(self):
        """Gets the type of this Activity.  # noqa: E501


        :return: The type of this Activity.  # noqa: E501
        :rtype: ActivityType
        """
        return self._type

    @type.setter
    def type(self, type):
        """Sets the type of this Activity.


        :param type: The type of this Activity.  # noqa: E501
        :type: ActivityType
        """
        if type is None:
            raise ValueError("Invalid value for `type`, must not be `None`")  # noqa: E501

        self._type = type

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, Activity):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
