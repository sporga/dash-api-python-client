# coding: utf-8

"""
    Dash API

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)  # noqa: E501

    OpenAPI spec version: v1
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


import pprint
import re  # noqa: F401

import six

from dash_api.models.event_status import EventStatus  # noqa: F401,E501
from dash_api.models.event_type import EventType  # noqa: F401,E501


class Event(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'id': 'int',
        'name': 'str',
        'start': 'datetime',
        'end': 'datetime',
        'status': 'EventStatus',
        'type': 'EventType'
    }

    attribute_map = {
        'id': 'id',
        'name': 'name',
        'start': 'start',
        'end': 'end',
        'status': 'status',
        'type': 'type'
    }

    def __init__(self, id=None, name=None, start=None, end=None, status=None, type=None):  # noqa: E501
        """Event - a model defined in Swagger"""  # noqa: E501

        self._id = None
        self._name = None
        self._start = None
        self._end = None
        self._status = None
        self._type = None
        self.discriminator = None

        if id is not None:
            self.id = id
        if name is not None:
            self.name = name
        if start is not None:
            self.start = start
        if end is not None:
            self.end = end
        if status is not None:
            self.status = status
        if type is not None:
            self.type = type

    @property
    def id(self):
        """Gets the id of this Event.  # noqa: E501


        :return: The id of this Event.  # noqa: E501
        :rtype: int
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this Event.


        :param id: The id of this Event.  # noqa: E501
        :type: int
        """

        self._id = id

    @property
    def name(self):
        """Gets the name of this Event.  # noqa: E501


        :return: The name of this Event.  # noqa: E501
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """Sets the name of this Event.


        :param name: The name of this Event.  # noqa: E501
        :type: str
        """

        self._name = name

    @property
    def start(self):
        """Gets the start of this Event.  # noqa: E501


        :return: The start of this Event.  # noqa: E501
        :rtype: datetime
        """
        return self._start

    @start.setter
    def start(self, start):
        """Sets the start of this Event.


        :param start: The start of this Event.  # noqa: E501
        :type: datetime
        """

        self._start = start

    @property
    def end(self):
        """Gets the end of this Event.  # noqa: E501


        :return: The end of this Event.  # noqa: E501
        :rtype: datetime
        """
        return self._end

    @end.setter
    def end(self, end):
        """Sets the end of this Event.


        :param end: The end of this Event.  # noqa: E501
        :type: datetime
        """

        self._end = end

    @property
    def status(self):
        """Gets the status of this Event.  # noqa: E501


        :return: The status of this Event.  # noqa: E501
        :rtype: EventStatus
        """
        return self._status

    @status.setter
    def status(self, status):
        """Sets the status of this Event.


        :param status: The status of this Event.  # noqa: E501
        :type: EventStatus
        """

        self._status = status

    @property
    def type(self):
        """Gets the type of this Event.  # noqa: E501


        :return: The type of this Event.  # noqa: E501
        :rtype: EventType
        """
        return self._type

    @type.setter
    def type(self, type):
        """Sets the type of this Event.


        :param type: The type of this Event.  # noqa: E501
        :type: EventType
        """

        self._type = type

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, Event):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
