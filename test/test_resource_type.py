# coding: utf-8

"""
    Dash API

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)  # noqa: E501

    OpenAPI spec version: v1
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from __future__ import absolute_import

import unittest

import dash_api
from dash_api.models.resource_type import ResourceType  # noqa: E501
from dash_api.rest import ApiException


class TestResourceType(unittest.TestCase):
    """ResourceType unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testResourceType(self):
        """Test ResourceType"""
        # FIXME: construct object with mandatory attributes with example values
        # model = dash_api.models.resource_type.ResourceType()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
