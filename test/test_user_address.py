# coding: utf-8

"""
    Dash API

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)  # noqa: E501

    OpenAPI spec version: v1
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from __future__ import absolute_import

import unittest

import dash_api
from dash_api.models.user_address import UserAddress  # noqa: E501
from dash_api.rest import ApiException


class TestUserAddress(unittest.TestCase):
    """UserAddress unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testUserAddress(self):
        """Test UserAddress"""
        # FIXME: construct object with mandatory attributes with example values
        # model = dash_api.models.user_address.UserAddress()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
