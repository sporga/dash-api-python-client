# Charge

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**first_name** | **str** |  | 
**surname** | **str** |  | 
**unique_identifier** | **str** |  | 
**gender** | **str** |  | [optional] 
**date_of_birth** | **datetime** |  | [optional] 
**organisations** | [**list[Organisation]**](Organisation.md) |  | [optional] 
**primary_organisation** | [**Organisation**](Organisation.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


