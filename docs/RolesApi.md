# dash_api.RolesApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**api_roles_by_id_delete**](RolesApi.md#api_roles_by_id_delete) | **DELETE** /api/roles/{id} | 
[**api_roles_by_id_get**](RolesApi.md#api_roles_by_id_get) | **GET** /api/roles/{id} | 
[**api_roles_by_id_put**](RolesApi.md#api_roles_by_id_put) | **PUT** /api/roles/{id} | 
[**api_roles_by_id_resource_types_by_resource_type_id_permissions_get**](RolesApi.md#api_roles_by_id_resource_types_by_resource_type_id_permissions_get) | **GET** /api/roles/{id}/resourceTypes/{resourceTypeId}/permissions | 
[**api_roles_by_id_resource_types_by_resource_type_id_permissions_post**](RolesApi.md#api_roles_by_id_resource_types_by_resource_type_id_permissions_post) | **POST** /api/roles/{id}/resourceTypes/{resourceTypeId}/permissions | 
[**api_roles_by_id_resource_types_get**](RolesApi.md#api_roles_by_id_resource_types_get) | **GET** /api/roles/{id}/resourceTypes | 
[**api_roles_by_id_resource_types_post**](RolesApi.md#api_roles_by_id_resource_types_post) | **POST** /api/roles/{id}/resourceTypes | 
[**api_roles_by_id_resources_by_resource_id_permissions_get**](RolesApi.md#api_roles_by_id_resources_by_resource_id_permissions_get) | **GET** /api/roles/{id}/resources/{resourceId}/permissions | 
[**api_roles_by_id_resources_by_resource_id_permissions_post**](RolesApi.md#api_roles_by_id_resources_by_resource_id_permissions_post) | **POST** /api/roles/{id}/resources/{resourceId}/permissions | 
[**api_roles_by_id_resources_get**](RolesApi.md#api_roles_by_id_resources_get) | **GET** /api/roles/{id}/resources | 
[**api_roles_by_id_resources_post**](RolesApi.md#api_roles_by_id_resources_post) | **POST** /api/roles/{id}/resources | 
[**api_roles_by_id_users_by_provider_id_get**](RolesApi.md#api_roles_by_id_users_by_provider_id_get) | **GET** /api/roles/{id}/users/{providerId} | 
[**api_roles_by_id_users_get**](RolesApi.md#api_roles_by_id_users_get) | **GET** /api/roles/{id}/users | 
[**api_roles_by_id_users_post**](RolesApi.md#api_roles_by_id_users_post) | **POST** /api/roles/{id}/users | 
[**api_roles_by_role_id_users_by_provider_id_restrictions_by_restriction_id_delete**](RolesApi.md#api_roles_by_role_id_users_by_provider_id_restrictions_by_restriction_id_delete) | **DELETE** /api/roles/{roleId}/users/{providerId}/restrictions/{restrictionId} | 
[**api_roles_by_role_id_users_by_provider_id_restrictions_get**](RolesApi.md#api_roles_by_role_id_users_by_provider_id_restrictions_get) | **GET** /api/roles/{roleId}/users/{providerId}/restrictions | 
[**api_roles_by_role_id_users_by_provider_id_restrictions_post**](RolesApi.md#api_roles_by_role_id_users_by_provider_id_restrictions_post) | **POST** /api/roles/{roleId}/users/{providerId}/restrictions | 
[**api_roles_get**](RolesApi.md#api_roles_get) | **GET** /api/roles | 
[**api_roles_post**](RolesApi.md#api_roles_post) | **POST** /api/roles | 


# **api_roles_by_id_delete**
> api_roles_by_id_delete(id)



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.RolesApi(dash_api.ApiClient(configuration))
id = 56 # int | 

try:
    api_instance.api_roles_by_id_delete(id)
except ApiException as e:
    print("Exception when calling RolesApi->api_roles_by_id_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  | 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_roles_by_id_get**
> Role api_roles_by_id_get(id)



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.RolesApi(dash_api.ApiClient(configuration))
id = 56 # int | 

try:
    api_response = api_instance.api_roles_by_id_get(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RolesApi->api_roles_by_id_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  | 

### Return type

[**Role**](Role.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_roles_by_id_put**
> api_roles_by_id_put(id, model=model)



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.RolesApi(dash_api.ApiClient(configuration))
id = 56 # int | 
model = dash_api.Role() # Role |  (optional)

try:
    api_instance.api_roles_by_id_put(id, model=model)
except ApiException as e:
    print("Exception when calling RolesApi->api_roles_by_id_put: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  | 
 **model** | [**Role**](Role.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/*+json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_roles_by_id_resource_types_by_resource_type_id_permissions_get**
> list[Permission] api_roles_by_id_resource_types_by_resource_type_id_permissions_get(id, resource_type_id)



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.RolesApi(dash_api.ApiClient(configuration))
id = 56 # int | 
resource_type_id = 56 # int | 

try:
    api_response = api_instance.api_roles_by_id_resource_types_by_resource_type_id_permissions_get(id, resource_type_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RolesApi->api_roles_by_id_resource_types_by_resource_type_id_permissions_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  | 
 **resource_type_id** | **int**|  | 

### Return type

[**list[Permission]**](Permission.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_roles_by_id_resource_types_by_resource_type_id_permissions_post**
> api_roles_by_id_resource_types_by_resource_type_id_permissions_post(id, resource_type_id, permissions=permissions)



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.RolesApi(dash_api.ApiClient(configuration))
id = 56 # int | 
resource_type_id = 56 # int | 
permissions = [dash_api.Permission()] # list[Permission] |  (optional)

try:
    api_instance.api_roles_by_id_resource_types_by_resource_type_id_permissions_post(id, resource_type_id, permissions=permissions)
except ApiException as e:
    print("Exception when calling RolesApi->api_roles_by_id_resource_types_by_resource_type_id_permissions_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  | 
 **resource_type_id** | **int**|  | 
 **permissions** | [**list[Permission]**](Permission.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/*+json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_roles_by_id_resource_types_get**
> list[ResourceType] api_roles_by_id_resource_types_get(id)



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.RolesApi(dash_api.ApiClient(configuration))
id = 56 # int | 

try:
    api_response = api_instance.api_roles_by_id_resource_types_get(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RolesApi->api_roles_by_id_resource_types_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  | 

### Return type

[**list[ResourceType]**](ResourceType.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_roles_by_id_resource_types_post**
> api_roles_by_id_resource_types_post(id, resource_types=resource_types)



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.RolesApi(dash_api.ApiClient(configuration))
id = 56 # int | 
resource_types = [dash_api.ResourceType()] # list[ResourceType] |  (optional)

try:
    api_instance.api_roles_by_id_resource_types_post(id, resource_types=resource_types)
except ApiException as e:
    print("Exception when calling RolesApi->api_roles_by_id_resource_types_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  | 
 **resource_types** | [**list[ResourceType]**](ResourceType.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/*+json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_roles_by_id_resources_by_resource_id_permissions_get**
> list[Permission] api_roles_by_id_resources_by_resource_id_permissions_get(id, resource_id)



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.RolesApi(dash_api.ApiClient(configuration))
id = 56 # int | 
resource_id = 56 # int | 

try:
    api_response = api_instance.api_roles_by_id_resources_by_resource_id_permissions_get(id, resource_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RolesApi->api_roles_by_id_resources_by_resource_id_permissions_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  | 
 **resource_id** | **int**|  | 

### Return type

[**list[Permission]**](Permission.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_roles_by_id_resources_by_resource_id_permissions_post**
> api_roles_by_id_resources_by_resource_id_permissions_post(id, resource_id, permissions=permissions)



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.RolesApi(dash_api.ApiClient(configuration))
id = 56 # int | 
resource_id = 56 # int | 
permissions = [dash_api.Permission()] # list[Permission] |  (optional)

try:
    api_instance.api_roles_by_id_resources_by_resource_id_permissions_post(id, resource_id, permissions=permissions)
except ApiException as e:
    print("Exception when calling RolesApi->api_roles_by_id_resources_by_resource_id_permissions_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  | 
 **resource_id** | **int**|  | 
 **permissions** | [**list[Permission]**](Permission.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/*+json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_roles_by_id_resources_get**
> list[Resource] api_roles_by_id_resources_get(id)



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.RolesApi(dash_api.ApiClient(configuration))
id = 56 # int | 

try:
    api_response = api_instance.api_roles_by_id_resources_get(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RolesApi->api_roles_by_id_resources_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  | 

### Return type

[**list[Resource]**](Resource.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_roles_by_id_resources_post**
> api_roles_by_id_resources_post(id, resources=resources)



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.RolesApi(dash_api.ApiClient(configuration))
id = 56 # int | 
resources = [dash_api.Resource()] # list[Resource] |  (optional)

try:
    api_instance.api_roles_by_id_resources_post(id, resources=resources)
except ApiException as e:
    print("Exception when calling RolesApi->api_roles_by_id_resources_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  | 
 **resources** | [**list[Resource]**](Resource.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/*+json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_roles_by_id_users_by_provider_id_get**
> User api_roles_by_id_users_by_provider_id_get(id, provider_id)



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.RolesApi(dash_api.ApiClient(configuration))
id = 56 # int | 
provider_id = 'provider_id_example' # str | 

try:
    api_response = api_instance.api_roles_by_id_users_by_provider_id_get(id, provider_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RolesApi->api_roles_by_id_users_by_provider_id_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  | 
 **provider_id** | **str**|  | 

### Return type

[**User**](User.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_roles_by_id_users_get**
> list[User] api_roles_by_id_users_get(id)



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.RolesApi(dash_api.ApiClient(configuration))
id = 56 # int | 

try:
    api_response = api_instance.api_roles_by_id_users_get(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RolesApi->api_roles_by_id_users_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  | 

### Return type

[**list[User]**](User.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_roles_by_id_users_post**
> api_roles_by_id_users_post(id, user=user)



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.RolesApi(dash_api.ApiClient(configuration))
id = 56 # int | 
user = dash_api.User() # User |  (optional)

try:
    api_instance.api_roles_by_id_users_post(id, user=user)
except ApiException as e:
    print("Exception when calling RolesApi->api_roles_by_id_users_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  | 
 **user** | [**User**](User.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/*+json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_roles_by_role_id_users_by_provider_id_restrictions_by_restriction_id_delete**
> api_roles_by_role_id_users_by_provider_id_restrictions_by_restriction_id_delete(role_id, provider_id, restriction_id)



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.RolesApi(dash_api.ApiClient(configuration))
role_id = 56 # int | 
provider_id = 'provider_id_example' # str | 
restriction_id = 56 # int | 

try:
    api_instance.api_roles_by_role_id_users_by_provider_id_restrictions_by_restriction_id_delete(role_id, provider_id, restriction_id)
except ApiException as e:
    print("Exception when calling RolesApi->api_roles_by_role_id_users_by_provider_id_restrictions_by_restriction_id_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **role_id** | **int**|  | 
 **provider_id** | **str**|  | 
 **restriction_id** | **int**|  | 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_roles_by_role_id_users_by_provider_id_restrictions_get**
> Restriction api_roles_by_role_id_users_by_provider_id_restrictions_get(role_id, provider_id)



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.RolesApi(dash_api.ApiClient(configuration))
role_id = 56 # int | 
provider_id = 'provider_id_example' # str | 

try:
    api_response = api_instance.api_roles_by_role_id_users_by_provider_id_restrictions_get(role_id, provider_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RolesApi->api_roles_by_role_id_users_by_provider_id_restrictions_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **role_id** | **int**|  | 
 **provider_id** | **str**|  | 

### Return type

[**Restriction**](Restriction.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_roles_by_role_id_users_by_provider_id_restrictions_post**
> api_roles_by_role_id_users_by_provider_id_restrictions_post(role_id, provider_id, restriction=restriction)



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.RolesApi(dash_api.ApiClient(configuration))
role_id = 56 # int | 
provider_id = 'provider_id_example' # str | 
restriction = dash_api.Restriction() # Restriction |  (optional)

try:
    api_instance.api_roles_by_role_id_users_by_provider_id_restrictions_post(role_id, provider_id, restriction=restriction)
except ApiException as e:
    print("Exception when calling RolesApi->api_roles_by_role_id_users_by_provider_id_restrictions_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **role_id** | **int**|  | 
 **provider_id** | **str**|  | 
 **restriction** | [**Restriction**](Restriction.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/*+json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_roles_get**
> list[Role] api_roles_get()



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.RolesApi(dash_api.ApiClient(configuration))

try:
    api_response = api_instance.api_roles_get()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RolesApi->api_roles_get: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**list[Role]**](Role.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_roles_post**
> Role api_roles_post(input=input)



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.RolesApi(dash_api.ApiClient(configuration))
input = dash_api.Role() # Role |  (optional)

try:
    api_response = api_instance.api_roles_post(input=input)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RolesApi->api_roles_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **input** | [**Role**](Role.md)|  | [optional] 

### Return type

[**Role**](Role.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/*+json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

