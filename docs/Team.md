# Team

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**name** | **str** |  | 
**user** | [**User**](User.md) |  | 
**organisation** | [**Organisation**](Organisation.md) |  | 
**activity** | [**Activity**](Activity.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


