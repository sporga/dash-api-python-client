# dash_api.UserApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**api_user_by_provider_id_charges_by_charge_id_get**](UserApi.md#api_user_by_provider_id_charges_by_charge_id_get) | **GET** /api/user/{providerId}/charges/{chargeId} | 
[**api_user_by_provider_id_charges_by_relationship_type_post**](UserApi.md#api_user_by_provider_id_charges_by_relationship_type_post) | **POST** /api/user/{providerId}/charges/{relationshipType} | 
[**api_user_by_provider_id_charges_get**](UserApi.md#api_user_by_provider_id_charges_get) | **GET** /api/user/{providerId}/charges | 
[**api_user_by_provider_id_get**](UserApi.md#api_user_by_provider_id_get) | **GET** /api/user/{providerId} | 
[**api_user_by_provider_id_head**](UserApi.md#api_user_by_provider_id_head) | **HEAD** /api/user/{providerId} | 
[**api_user_by_provider_id_organisations_by_assignment_post**](UserApi.md#api_user_by_provider_id_organisations_by_assignment_post) | **POST** /api/user/{providerId}/organisations/{assignment} | 
[**api_user_by_provider_id_organisations_by_organisation_id_delete**](UserApi.md#api_user_by_provider_id_organisations_by_organisation_id_delete) | **DELETE** /api/user/{providerId}/organisations/{organisationId} | 
[**api_user_by_provider_id_organisations_by_organisation_id_get**](UserApi.md#api_user_by_provider_id_organisations_by_organisation_id_get) | **GET** /api/user/{providerId}/organisations/{organisationId} | 
[**api_user_by_provider_id_organisations_get**](UserApi.md#api_user_by_provider_id_organisations_get) | **GET** /api/user/{providerId}/organisations | 
[**api_user_get**](UserApi.md#api_user_get) | **GET** /api/user | 
[**api_user_post**](UserApi.md#api_user_post) | **POST** /api/user | 


# **api_user_by_provider_id_charges_by_charge_id_get**
> Charge api_user_by_provider_id_charges_by_charge_id_get(provider_id, charge_id)



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.UserApi(dash_api.ApiClient(configuration))
provider_id = 'provider_id_example' # str | 
charge_id = 56 # int | 

try:
    api_response = api_instance.api_user_by_provider_id_charges_by_charge_id_get(provider_id, charge_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling UserApi->api_user_by_provider_id_charges_by_charge_id_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **provider_id** | **str**|  | 
 **charge_id** | **int**|  | 

### Return type

[**Charge**](Charge.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_user_by_provider_id_charges_by_relationship_type_post**
> api_user_by_provider_id_charges_by_relationship_type_post(relationship, provider_id, relationship_type, charge=charge)



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.UserApi(dash_api.ApiClient(configuration))
relationship = 'relationship_example' # str | 
provider_id = 'provider_id_example' # str | 
relationship_type = 'relationship_type_example' # str | 
charge = dash_api.Charge() # Charge |  (optional)

try:
    api_instance.api_user_by_provider_id_charges_by_relationship_type_post(relationship, provider_id, relationship_type, charge=charge)
except ApiException as e:
    print("Exception when calling UserApi->api_user_by_provider_id_charges_by_relationship_type_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **relationship** | **str**|  | 
 **provider_id** | **str**|  | 
 **relationship_type** | **str**|  | 
 **charge** | [**Charge**](Charge.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/*+json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_user_by_provider_id_charges_get**
> list[Charge] api_user_by_provider_id_charges_get(provider_id, relationship=relationship)



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.UserApi(dash_api.ApiClient(configuration))
provider_id = 'provider_id_example' # str | 
relationship = 'relationship_example' # str |  (optional)

try:
    api_response = api_instance.api_user_by_provider_id_charges_get(provider_id, relationship=relationship)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling UserApi->api_user_by_provider_id_charges_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **provider_id** | **str**|  | 
 **relationship** | **str**|  | [optional] 

### Return type

[**list[Charge]**](Charge.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_user_by_provider_id_get**
> User api_user_by_provider_id_get(provider_id)



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.UserApi(dash_api.ApiClient(configuration))
provider_id = 'provider_id_example' # str | 

try:
    api_response = api_instance.api_user_by_provider_id_get(provider_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling UserApi->api_user_by_provider_id_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **provider_id** | **str**|  | 

### Return type

[**User**](User.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_user_by_provider_id_head**
> api_user_by_provider_id_head(provider_id)



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.UserApi(dash_api.ApiClient(configuration))
provider_id = 'provider_id_example' # str | 

try:
    api_instance.api_user_by_provider_id_head(provider_id)
except ApiException as e:
    print("Exception when calling UserApi->api_user_by_provider_id_head: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **provider_id** | **str**|  | 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_user_by_provider_id_organisations_by_assignment_post**
> api_user_by_provider_id_organisations_by_assignment_post(provider_id, assignment, organisation=organisation)



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.UserApi(dash_api.ApiClient(configuration))
provider_id = 'provider_id_example' # str | 
assignment = 'assignment_example' # str | 
organisation = dash_api.Organisation() # Organisation |  (optional)

try:
    api_instance.api_user_by_provider_id_organisations_by_assignment_post(provider_id, assignment, organisation=organisation)
except ApiException as e:
    print("Exception when calling UserApi->api_user_by_provider_id_organisations_by_assignment_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **provider_id** | **str**|  | 
 **assignment** | **str**|  | 
 **organisation** | [**Organisation**](Organisation.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/*+json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_user_by_provider_id_organisations_by_organisation_id_delete**
> api_user_by_provider_id_organisations_by_organisation_id_delete(provider_id, organisation_id)



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.UserApi(dash_api.ApiClient(configuration))
provider_id = 'provider_id_example' # str | 
organisation_id = 56 # int | 

try:
    api_instance.api_user_by_provider_id_organisations_by_organisation_id_delete(provider_id, organisation_id)
except ApiException as e:
    print("Exception when calling UserApi->api_user_by_provider_id_organisations_by_organisation_id_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **provider_id** | **str**|  | 
 **organisation_id** | **int**|  | 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_user_by_provider_id_organisations_by_organisation_id_get**
> Organisation api_user_by_provider_id_organisations_by_organisation_id_get(provider_id, organisation_id)



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.UserApi(dash_api.ApiClient(configuration))
provider_id = 'provider_id_example' # str | 
organisation_id = 56 # int | 

try:
    api_response = api_instance.api_user_by_provider_id_organisations_by_organisation_id_get(provider_id, organisation_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling UserApi->api_user_by_provider_id_organisations_by_organisation_id_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **provider_id** | **str**|  | 
 **organisation_id** | **int**|  | 

### Return type

[**Organisation**](Organisation.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_user_by_provider_id_organisations_get**
> list[Organisation] api_user_by_provider_id_organisations_get(provider_id, assignments=assignments)



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.UserApi(dash_api.ApiClient(configuration))
provider_id = 'provider_id_example' # str | 
assignments = ['assignments_example'] # list[str] |  (optional)

try:
    api_response = api_instance.api_user_by_provider_id_organisations_get(provider_id, assignments=assignments)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling UserApi->api_user_by_provider_id_organisations_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **provider_id** | **str**|  | 
 **assignments** | [**list[str]**](str.md)|  | [optional] 

### Return type

[**list[Organisation]**](Organisation.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_user_get**
> list[User] api_user_get(given_name=given_name, last_name=last_name)



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.UserApi(dash_api.ApiClient(configuration))
given_name = 'given_name_example' # str |  (optional)
last_name = 'last_name_example' # str |  (optional)

try:
    api_response = api_instance.api_user_get(given_name=given_name, last_name=last_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling UserApi->api_user_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **given_name** | **str**|  | [optional] 
 **last_name** | **str**|  | [optional] 

### Return type

[**list[User]**](User.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_user_post**
> User api_user_post(model=model)



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.UserApi(dash_api.ApiClient(configuration))
model = dash_api.User() # User |  (optional)

try:
    api_response = api_instance.api_user_post(model=model)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling UserApi->api_user_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **model** | [**User**](User.md)|  | [optional] 

### Return type

[**User**](User.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/*+json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

