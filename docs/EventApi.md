# dash_api.EventApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**api_events_by_id_activities_get**](EventApi.md#api_events_by_id_activities_get) | **GET** /api/events/{Id}/activities | 
[**api_events_by_id_activities_post**](EventApi.md#api_events_by_id_activities_post) | **POST** /api/events/{Id}/activities | 
[**api_events_by_id_get**](EventApi.md#api_events_by_id_get) | **GET** /api/events/{Id} | 
[**api_events_by_id_organisations_get**](EventApi.md#api_events_by_id_organisations_get) | **GET** /api/events/{Id}/organisations | 
[**api_events_by_id_organisations_post**](EventApi.md#api_events_by_id_organisations_post) | **POST** /api/events/{Id}/organisations | 
[**api_events_get**](EventApi.md#api_events_get) | **GET** /api/events | 
[**api_events_post**](EventApi.md#api_events_post) | **POST** /api/events | 


# **api_events_by_id_activities_get**
> api_events_by_id_activities_get(id)



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.EventApi(dash_api.ApiClient(configuration))
id = 56 # int | 

try:
    api_instance.api_events_by_id_activities_get(id)
except ApiException as e:
    print("Exception when calling EventApi->api_events_by_id_activities_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  | 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_events_by_id_activities_post**
> api_events_by_id_activities_post(id, activities=activities)



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.EventApi(dash_api.ApiClient(configuration))
id = 56 # int | 
activities = [dash_api.Activity()] # list[Activity] |  (optional)

try:
    api_instance.api_events_by_id_activities_post(id, activities=activities)
except ApiException as e:
    print("Exception when calling EventApi->api_events_by_id_activities_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  | 
 **activities** | [**list[Activity]**](Activity.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/*+json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_events_by_id_get**
> api_events_by_id_get(id)



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.EventApi(dash_api.ApiClient(configuration))
id = 56 # int | 

try:
    api_instance.api_events_by_id_get(id)
except ApiException as e:
    print("Exception when calling EventApi->api_events_by_id_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  | 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_events_by_id_organisations_get**
> api_events_by_id_organisations_get(id)



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.EventApi(dash_api.ApiClient(configuration))
id = 56 # int | 

try:
    api_instance.api_events_by_id_organisations_get(id)
except ApiException as e:
    print("Exception when calling EventApi->api_events_by_id_organisations_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  | 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_events_by_id_organisations_post**
> api_events_by_id_organisations_post(id, organisations=organisations)



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.EventApi(dash_api.ApiClient(configuration))
id = 56 # int | 
organisations = dash_api.PrimaryAffiliateOrganisation() # PrimaryAffiliateOrganisation |  (optional)

try:
    api_instance.api_events_by_id_organisations_post(id, organisations=organisations)
except ApiException as e:
    print("Exception when calling EventApi->api_events_by_id_organisations_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  | 
 **organisations** | [**PrimaryAffiliateOrganisation**](PrimaryAffiliateOrganisation.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/*+json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_events_get**
> api_events_get()



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.EventApi(dash_api.ApiClient(configuration))

try:
    api_instance.api_events_get()
except ApiException as e:
    print("Exception when calling EventApi->api_events_get: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_events_post**
> api_events_post(model=model)



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.EventApi(dash_api.ApiClient(configuration))
model = dash_api.Event() # Event |  (optional)

try:
    api_instance.api_events_post(model=model)
except ApiException as e:
    print("Exception when calling EventApi->api_events_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **model** | [**Event**](Event.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/*+json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

