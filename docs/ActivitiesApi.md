# dash_api.ActivitiesApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**api_activities_by_id_get**](ActivitiesApi.md#api_activities_by_id_get) | **GET** /api/activities/{id} | 
[**api_activities_get**](ActivitiesApi.md#api_activities_get) | **GET** /api/activities | 
[**api_activities_post**](ActivitiesApi.md#api_activities_post) | **POST** /api/activities | 
[**api_activities_structure_by_id_get**](ActivitiesApi.md#api_activities_structure_by_id_get) | **GET** /api/activities/structure/{id} | 
[**api_activities_top_most_get**](ActivitiesApi.md#api_activities_top_most_get) | **GET** /api/activities/top-most | 
[**api_activities_types_by_id_get**](ActivitiesApi.md#api_activities_types_by_id_get) | **GET** /api/activities/types/{id} | 
[**api_activities_types_get**](ActivitiesApi.md#api_activities_types_get) | **GET** /api/activities/types | 
[**api_activities_types_post**](ActivitiesApi.md#api_activities_types_post) | **POST** /api/activities/types | 


# **api_activities_by_id_get**
> Activity api_activities_by_id_get(id)



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.ActivitiesApi(dash_api.ApiClient(configuration))
id = 56 # int | 

try:
    api_response = api_instance.api_activities_by_id_get(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ActivitiesApi->api_activities_by_id_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  | 

### Return type

[**Activity**](Activity.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_activities_get**
> list[Activity] api_activities_get()



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.ActivitiesApi(dash_api.ApiClient(configuration))

try:
    api_response = api_instance.api_activities_get()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ActivitiesApi->api_activities_get: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**list[Activity]**](Activity.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_activities_post**
> Activity api_activities_post(activity=activity)



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.ActivitiesApi(dash_api.ApiClient(configuration))
activity = dash_api.Activity() # Activity |  (optional)

try:
    api_response = api_instance.api_activities_post(activity=activity)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ActivitiesApi->api_activities_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **activity** | [**Activity**](Activity.md)|  | [optional] 

### Return type

[**Activity**](Activity.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/*+json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_activities_structure_by_id_get**
> api_activities_structure_by_id_get(id)



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.ActivitiesApi(dash_api.ApiClient(configuration))
id = 56 # int | 

try:
    api_instance.api_activities_structure_by_id_get(id)
except ApiException as e:
    print("Exception when calling ActivitiesApi->api_activities_structure_by_id_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  | 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_activities_top_most_get**
> list[Activity] api_activities_top_most_get()



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.ActivitiesApi(dash_api.ApiClient(configuration))

try:
    api_response = api_instance.api_activities_top_most_get()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ActivitiesApi->api_activities_top_most_get: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**list[Activity]**](Activity.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_activities_types_by_id_get**
> ActivityType api_activities_types_by_id_get(id)



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.ActivitiesApi(dash_api.ApiClient(configuration))
id = 56 # int | 

try:
    api_response = api_instance.api_activities_types_by_id_get(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ActivitiesApi->api_activities_types_by_id_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  | 

### Return type

[**ActivityType**](ActivityType.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_activities_types_get**
> list[ActivityType] api_activities_types_get()



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.ActivitiesApi(dash_api.ApiClient(configuration))

try:
    api_response = api_instance.api_activities_types_get()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ActivitiesApi->api_activities_types_get: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**list[ActivityType]**](ActivityType.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_activities_types_post**
> ActivityType api_activities_types_post(model=model)



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.ActivitiesApi(dash_api.ApiClient(configuration))
model = dash_api.ActivityType() # ActivityType |  (optional)

try:
    api_response = api_instance.api_activities_types_post(model=model)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ActivitiesApi->api_activities_types_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **model** | [**ActivityType**](ActivityType.md)|  | [optional] 

### Return type

[**ActivityType**](ActivityType.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/*+json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

