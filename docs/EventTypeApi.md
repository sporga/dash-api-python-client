# dash_api.EventTypeApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**api_events_types_get**](EventTypeApi.md#api_events_types_get) | **GET** /api/events/types | 


# **api_events_types_get**
> api_events_types_get()



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.EventTypeApi(dash_api.ApiClient(configuration))

try:
    api_instance.api_events_types_get()
except ApiException as e:
    print("Exception when calling EventTypeApi->api_events_types_get: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

