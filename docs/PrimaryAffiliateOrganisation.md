# PrimaryAffiliateOrganisation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**primary** | [**Organisation**](Organisation.md) |  | [optional] 
**affiliates** | [**list[Organisation]**](Organisation.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


