# Organisation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**parent** | [**Organisation**](Organisation.md) |  | [optional] 
**address** | [**Address**](Address.md) |  | [optional] 
**contact** | [**OrganisationContact**](OrganisationContact.md) |  | [optional] 
**type** | [**OrganisationType**](OrganisationType.md) |  | [optional] 
**name** | **str** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


