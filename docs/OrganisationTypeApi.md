# dash_api.OrganisationTypeApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**api_organisation_type_get**](OrganisationTypeApi.md#api_organisation_type_get) | **GET** /api/organisation/type | 


# **api_organisation_type_get**
> api_organisation_type_get()



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.OrganisationTypeApi(dash_api.ApiClient(configuration))

try:
    api_instance.api_organisation_type_get()
except ApiException as e:
    print("Exception when calling OrganisationTypeApi->api_organisation_type_get: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

