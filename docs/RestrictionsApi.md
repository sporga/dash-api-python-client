# dash_api.RestrictionsApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**api_restrictions_by_id_delete**](RestrictionsApi.md#api_restrictions_by_id_delete) | **DELETE** /api/Restrictions/{id} | 
[**api_restrictions_by_id_get**](RestrictionsApi.md#api_restrictions_by_id_get) | **GET** /api/Restrictions/{id} | 
[**api_restrictions_post**](RestrictionsApi.md#api_restrictions_post) | **POST** /api/Restrictions | 


# **api_restrictions_by_id_delete**
> api_restrictions_by_id_delete(id)



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.RestrictionsApi(dash_api.ApiClient(configuration))
id = 56 # int | 

try:
    api_instance.api_restrictions_by_id_delete(id)
except ApiException as e:
    print("Exception when calling RestrictionsApi->api_restrictions_by_id_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  | 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_restrictions_by_id_get**
> Restriction api_restrictions_by_id_get(id)



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.RestrictionsApi(dash_api.ApiClient(configuration))
id = 56 # int | 

try:
    api_response = api_instance.api_restrictions_by_id_get(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RestrictionsApi->api_restrictions_by_id_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  | 

### Return type

[**Restriction**](Restriction.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_restrictions_post**
> Restriction api_restrictions_post(input=input)



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.RestrictionsApi(dash_api.ApiClient(configuration))
input = dash_api.Restriction() # Restriction |  (optional)

try:
    api_response = api_instance.api_restrictions_post(input=input)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling RestrictionsApi->api_restrictions_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **input** | [**Restriction**](Restriction.md)|  | [optional] 

### Return type

[**Restriction**](Restriction.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/*+json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

