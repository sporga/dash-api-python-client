# dash_api.TeamsApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**api_teams_by_id_delete**](TeamsApi.md#api_teams_by_id_delete) | **DELETE** /api/Teams/{id} | 
[**api_teams_by_id_get**](TeamsApi.md#api_teams_by_id_get) | **GET** /api/Teams/{id} | 
[**api_teams_by_id_put**](TeamsApi.md#api_teams_by_id_put) | **PUT** /api/Teams/{id} | 
[**api_teams_by_team_id_charges_by_charge_id_get**](TeamsApi.md#api_teams_by_team_id_charges_by_charge_id_get) | **GET** /api/Teams/{teamId}/charges/{chargeId} | 
[**api_teams_by_team_id_charges_get**](TeamsApi.md#api_teams_by_team_id_charges_get) | **GET** /api/Teams/{teamId}/charges | 
[**api_teams_by_team_id_charges_post**](TeamsApi.md#api_teams_by_team_id_charges_post) | **POST** /api/Teams/{teamId}/charges | 
[**api_teams_by_team_id_events_by_event_id_get**](TeamsApi.md#api_teams_by_team_id_events_by_event_id_get) | **GET** /api/Teams/{teamId}/events/{eventId} | 
[**api_teams_by_team_id_events_get**](TeamsApi.md#api_teams_by_team_id_events_get) | **GET** /api/Teams/{teamId}/events | 
[**api_teams_by_team_id_events_post**](TeamsApi.md#api_teams_by_team_id_events_post) | **POST** /api/Teams/{teamId}/events | 
[**api_teams_get**](TeamsApi.md#api_teams_get) | **GET** /api/Teams | 
[**api_teams_post**](TeamsApi.md#api_teams_post) | **POST** /api/Teams | 


# **api_teams_by_id_delete**
> api_teams_by_id_delete(id)



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.TeamsApi(dash_api.ApiClient(configuration))
id = 56 # int | 

try:
    api_instance.api_teams_by_id_delete(id)
except ApiException as e:
    print("Exception when calling TeamsApi->api_teams_by_id_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  | 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_teams_by_id_get**
> Team api_teams_by_id_get(id)



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.TeamsApi(dash_api.ApiClient(configuration))
id = 56 # int | 

try:
    api_response = api_instance.api_teams_by_id_get(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling TeamsApi->api_teams_by_id_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  | 

### Return type

[**Team**](Team.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_teams_by_id_put**
> api_teams_by_id_put(id, model=model)



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.TeamsApi(dash_api.ApiClient(configuration))
id = 56 # int | 
model = dash_api.Team() # Team |  (optional)

try:
    api_instance.api_teams_by_id_put(id, model=model)
except ApiException as e:
    print("Exception when calling TeamsApi->api_teams_by_id_put: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  | 
 **model** | [**Team**](Team.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/*+json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_teams_by_team_id_charges_by_charge_id_get**
> Charge api_teams_by_team_id_charges_by_charge_id_get(team_id, charge_id)



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.TeamsApi(dash_api.ApiClient(configuration))
team_id = 56 # int | 
charge_id = 56 # int | 

try:
    api_response = api_instance.api_teams_by_team_id_charges_by_charge_id_get(team_id, charge_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling TeamsApi->api_teams_by_team_id_charges_by_charge_id_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **team_id** | **int**|  | 
 **charge_id** | **int**|  | 

### Return type

[**Charge**](Charge.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_teams_by_team_id_charges_get**
> list[Charge] api_teams_by_team_id_charges_get(team_id)



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.TeamsApi(dash_api.ApiClient(configuration))
team_id = 56 # int | 

try:
    api_response = api_instance.api_teams_by_team_id_charges_get(team_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling TeamsApi->api_teams_by_team_id_charges_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **team_id** | **int**|  | 

### Return type

[**list[Charge]**](Charge.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_teams_by_team_id_charges_post**
> api_teams_by_team_id_charges_post(team_id, model=model)



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.TeamsApi(dash_api.ApiClient(configuration))
team_id = 56 # int | 
model = dash_api.Charge() # Charge |  (optional)

try:
    api_instance.api_teams_by_team_id_charges_post(team_id, model=model)
except ApiException as e:
    print("Exception when calling TeamsApi->api_teams_by_team_id_charges_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **team_id** | **int**|  | 
 **model** | [**Charge**](Charge.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/*+json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_teams_by_team_id_events_by_event_id_get**
> Event api_teams_by_team_id_events_by_event_id_get(team_id, event_id)



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.TeamsApi(dash_api.ApiClient(configuration))
team_id = 56 # int | 
event_id = 56 # int | 

try:
    api_response = api_instance.api_teams_by_team_id_events_by_event_id_get(team_id, event_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling TeamsApi->api_teams_by_team_id_events_by_event_id_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **team_id** | **int**|  | 
 **event_id** | **int**|  | 

### Return type

[**Event**](Event.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_teams_by_team_id_events_get**
> list[Event] api_teams_by_team_id_events_get(team_id)



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.TeamsApi(dash_api.ApiClient(configuration))
team_id = 56 # int | 

try:
    api_response = api_instance.api_teams_by_team_id_events_get(team_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling TeamsApi->api_teams_by_team_id_events_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **team_id** | **int**|  | 

### Return type

[**list[Event]**](Event.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_teams_by_team_id_events_post**
> api_teams_by_team_id_events_post(team_id, model=model)



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.TeamsApi(dash_api.ApiClient(configuration))
team_id = 56 # int | 
model = [dash_api.Event()] # list[Event] |  (optional)

try:
    api_instance.api_teams_by_team_id_events_post(team_id, model=model)
except ApiException as e:
    print("Exception when calling TeamsApi->api_teams_by_team_id_events_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **team_id** | **int**|  | 
 **model** | [**list[Event]**](Event.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/*+json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_teams_get**
> list[Team] api_teams_get()



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.TeamsApi(dash_api.ApiClient(configuration))

try:
    api_response = api_instance.api_teams_get()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling TeamsApi->api_teams_get: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**list[Team]**](Team.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_teams_post**
> Team api_teams_post(model=model)



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.TeamsApi(dash_api.ApiClient(configuration))
model = dash_api.Team() # Team |  (optional)

try:
    api_response = api_instance.api_teams_post(model=model)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling TeamsApi->api_teams_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **model** | [**Team**](Team.md)|  | [optional] 

### Return type

[**Team**](Team.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/*+json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

