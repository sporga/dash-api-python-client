# dash_api.OrganisationApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**api_organisation_by_id_get**](OrganisationApi.md#api_organisation_by_id_get) | **GET** /api/organisation/{id} | 
[**api_organisation_by_org_id_groups_by_org_group_id_delete**](OrganisationApi.md#api_organisation_by_org_id_groups_by_org_group_id_delete) | **DELETE** /api/organisation/{orgId}/groups/{orgGroupId} | 
[**api_organisation_by_org_id_groups_by_org_group_id_get**](OrganisationApi.md#api_organisation_by_org_id_groups_by_org_group_id_get) | **GET** /api/organisation/{orgId}/groups/{orgGroupId} | 
[**api_organisation_get**](OrganisationApi.md#api_organisation_get) | **GET** /api/organisation | 
[**api_organisation_groups_by_id_delete**](OrganisationApi.md#api_organisation_groups_by_id_delete) | **DELETE** /api/organisation/groups/{id} | 
[**api_organisation_groups_by_id_get**](OrganisationApi.md#api_organisation_groups_by_id_get) | **GET** /api/organisation/groups/{id} | 
[**api_organisation_groups_by_org_group_id_organisations_get**](OrganisationApi.md#api_organisation_groups_by_org_group_id_organisations_get) | **GET** /api/organisation/groups/{orgGroupId}/organisations | 
[**api_organisation_groups_by_org_group_id_organisations_post**](OrganisationApi.md#api_organisation_groups_by_org_group_id_organisations_post) | **POST** /api/organisation/groups/{orgGroupId}/organisations | 
[**api_organisation_groups_get**](OrganisationApi.md#api_organisation_groups_get) | **GET** /api/organisation/groups | 
[**api_organisation_groups_post**](OrganisationApi.md#api_organisation_groups_post) | **POST** /api/organisation/groups | 
[**api_organisation_post**](OrganisationApi.md#api_organisation_post) | **POST** /api/organisation | 
[**api_organisation_structure_by_org_id_get**](OrganisationApi.md#api_organisation_structure_by_org_id_get) | **GET** /api/organisation/structure/{orgId} | 
[**api_organisation_top_most_get**](OrganisationApi.md#api_organisation_top_most_get) | **GET** /api/organisation/top-most | 


# **api_organisation_by_id_get**
> api_organisation_by_id_get(id)



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.OrganisationApi(dash_api.ApiClient(configuration))
id = 56 # int | 

try:
    api_instance.api_organisation_by_id_get(id)
except ApiException as e:
    print("Exception when calling OrganisationApi->api_organisation_by_id_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  | 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_organisation_by_org_id_groups_by_org_group_id_delete**
> api_organisation_by_org_id_groups_by_org_group_id_delete(org_group_id, org_id)



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.OrganisationApi(dash_api.ApiClient(configuration))
org_group_id = 56 # int | 
org_id = 56 # int | 

try:
    api_instance.api_organisation_by_org_id_groups_by_org_group_id_delete(org_group_id, org_id)
except ApiException as e:
    print("Exception when calling OrganisationApi->api_organisation_by_org_id_groups_by_org_group_id_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_group_id** | **int**|  | 
 **org_id** | **int**|  | 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_organisation_by_org_id_groups_by_org_group_id_get**
> Organisation api_organisation_by_org_id_groups_by_org_group_id_get(org_group_id, org_id)



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.OrganisationApi(dash_api.ApiClient(configuration))
org_group_id = 56 # int | 
org_id = 56 # int | 

try:
    api_response = api_instance.api_organisation_by_org_id_groups_by_org_group_id_get(org_group_id, org_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling OrganisationApi->api_organisation_by_org_id_groups_by_org_group_id_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_group_id** | **int**|  | 
 **org_id** | **int**|  | 

### Return type

[**Organisation**](Organisation.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_organisation_get**
> api_organisation_get()



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.OrganisationApi(dash_api.ApiClient(configuration))

try:
    api_instance.api_organisation_get()
except ApiException as e:
    print("Exception when calling OrganisationApi->api_organisation_get: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_organisation_groups_by_id_delete**
> api_organisation_groups_by_id_delete(id)



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.OrganisationApi(dash_api.ApiClient(configuration))
id = 56 # int | 

try:
    api_instance.api_organisation_groups_by_id_delete(id)
except ApiException as e:
    print("Exception when calling OrganisationApi->api_organisation_groups_by_id_delete: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  | 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_organisation_groups_by_id_get**
> OrganisationGroup api_organisation_groups_by_id_get(id)



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.OrganisationApi(dash_api.ApiClient(configuration))
id = 56 # int | 

try:
    api_response = api_instance.api_organisation_groups_by_id_get(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling OrganisationApi->api_organisation_groups_by_id_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  | 

### Return type

[**OrganisationGroup**](OrganisationGroup.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_organisation_groups_by_org_group_id_organisations_get**
> list[Organisation] api_organisation_groups_by_org_group_id_organisations_get(org_group_id)



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.OrganisationApi(dash_api.ApiClient(configuration))
org_group_id = 56 # int | 

try:
    api_response = api_instance.api_organisation_groups_by_org_group_id_organisations_get(org_group_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling OrganisationApi->api_organisation_groups_by_org_group_id_organisations_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_group_id** | **int**|  | 

### Return type

[**list[Organisation]**](Organisation.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_organisation_groups_by_org_group_id_organisations_post**
> api_organisation_groups_by_org_group_id_organisations_post(org_group_id, organisation=organisation)



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.OrganisationApi(dash_api.ApiClient(configuration))
org_group_id = 56 # int | 
organisation = dash_api.Organisation() # Organisation |  (optional)

try:
    api_instance.api_organisation_groups_by_org_group_id_organisations_post(org_group_id, organisation=organisation)
except ApiException as e:
    print("Exception when calling OrganisationApi->api_organisation_groups_by_org_group_id_organisations_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_group_id** | **int**|  | 
 **organisation** | [**Organisation**](Organisation.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/*+json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_organisation_groups_get**
> list[OrganisationGroup] api_organisation_groups_get(name=name)



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.OrganisationApi(dash_api.ApiClient(configuration))
name = 'name_example' # str |  (optional)

try:
    api_response = api_instance.api_organisation_groups_get(name=name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling OrganisationApi->api_organisation_groups_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **str**|  | [optional] 

### Return type

[**list[OrganisationGroup]**](OrganisationGroup.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_organisation_groups_post**
> OrganisationGroup api_organisation_groups_post(input=input)



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.OrganisationApi(dash_api.ApiClient(configuration))
input = dash_api.OrganisationGroup() # OrganisationGroup |  (optional)

try:
    api_response = api_instance.api_organisation_groups_post(input=input)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling OrganisationApi->api_organisation_groups_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **input** | [**OrganisationGroup**](OrganisationGroup.md)|  | [optional] 

### Return type

[**OrganisationGroup**](OrganisationGroup.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/*+json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_organisation_post**
> api_organisation_post(input=input)



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.OrganisationApi(dash_api.ApiClient(configuration))
input = dash_api.Organisation() # Organisation |  (optional)

try:
    api_instance.api_organisation_post(input=input)
except ApiException as e:
    print("Exception when calling OrganisationApi->api_organisation_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **input** | [**Organisation**](Organisation.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/*+json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_organisation_structure_by_org_id_get**
> api_organisation_structure_by_org_id_get(org_id)



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.OrganisationApi(dash_api.ApiClient(configuration))
org_id = 56 # int | 

try:
    api_instance.api_organisation_structure_by_org_id_get(org_id)
except ApiException as e:
    print("Exception when calling OrganisationApi->api_organisation_structure_by_org_id_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **org_id** | **int**|  | 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_organisation_top_most_get**
> list[Organisation] api_organisation_top_most_get()



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.OrganisationApi(dash_api.ApiClient(configuration))

try:
    api_response = api_instance.api_organisation_top_most_get()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling OrganisationApi->api_organisation_top_most_get: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**list[Organisation]**](Organisation.md)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

