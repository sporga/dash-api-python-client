# dash_api.ChargeApi

All URIs are relative to *https://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**api_charge_by_id_get**](ChargeApi.md#api_charge_by_id_get) | **GET** /api/charge/{id} | 
[**api_charge_get**](ChargeApi.md#api_charge_get) | **GET** /api/charge | 
[**api_charge_identifier_get**](ChargeApi.md#api_charge_identifier_get) | **GET** /api/charge/identifier | 
[**api_charge_post**](ChargeApi.md#api_charge_post) | **POST** /api/charge | 


# **api_charge_by_id_get**
> api_charge_by_id_get(id)



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.ChargeApi(dash_api.ApiClient(configuration))
id = 56 # int | 

try:
    api_instance.api_charge_by_id_get(id)
except ApiException as e:
    print("Exception when calling ChargeApi->api_charge_by_id_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**|  | 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_charge_get**
> api_charge_get(first_name=first_name, surname=surname, date_of_birth=date_of_birth)



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.ChargeApi(dash_api.ApiClient(configuration))
first_name = 'first_name_example' # str |  (optional)
surname = 'surname_example' # str |  (optional)
date_of_birth = '2013-10-20T19:20:30+01:00' # datetime |  (optional)

try:
    api_instance.api_charge_get(first_name=first_name, surname=surname, date_of_birth=date_of_birth)
except ApiException as e:
    print("Exception when calling ChargeApi->api_charge_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **first_name** | **str**|  | [optional] 
 **surname** | **str**|  | [optional] 
 **date_of_birth** | **datetime**|  | [optional] 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_charge_identifier_get**
> api_charge_identifier_get(unique_identifier=unique_identifier)



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.ChargeApi(dash_api.ApiClient(configuration))
unique_identifier = 'unique_identifier_example' # str |  (optional)

try:
    api_instance.api_charge_identifier_get(unique_identifier=unique_identifier)
except ApiException as e:
    print("Exception when calling ChargeApi->api_charge_identifier_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **unique_identifier** | **str**|  | [optional] 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_charge_post**
> api_charge_post(input=input)



### Example
```python
from __future__ import print_function
import time
import dash_api
from dash_api.rest import ApiException
from pprint import pprint

# Configure OAuth2 access token for authorization: oauth2
configuration = dash_api.Configuration()
configuration.access_token = 'YOUR_ACCESS_TOKEN'

# create an instance of the API class
api_instance = dash_api.ChargeApi(dash_api.ApiClient(configuration))
input = dash_api.Charge() # Charge |  (optional)

try:
    api_instance.api_charge_post(input=input)
except ApiException as e:
    print("Exception when calling ChargeApi->api_charge_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **input** | [**Charge**](Charge.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json-patch+json, application/json, text/json, application/*+json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

