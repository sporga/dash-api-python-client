# UserAddress

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**street_address** | **str** |  | [optional] 
**locality** | **str** |  | [optional] 
**region** | **str** |  | [optional] 
**post_code** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


